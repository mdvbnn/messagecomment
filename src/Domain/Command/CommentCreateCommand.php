<?php

namespace BNNVARA\Comment\Domain\Command;

use BNNVARA\Comment\Domain\ValueObject\CommentCreate;

class CommentCreateCommand
{
  private CommentCreate $data;

  public function __construct(CommentCreate $data)
  {
    $this->data = $data;
  }

  public function getData(): CommentCreate
  {
    return $this->data;
  }

}