<?php

namespace BNNVARA\Comment\Domain\ValueObject;

use DateTime;

class CommentCreate
{
  private string $username, $comment;
  private DateTime $created;

  public function __construct(string $comment, string $username, DateTime $created)
  {
    $this->username = $username;
    $this->comment = $comment;
    $this->created = $created;
  }

  public function getUsername(): string
  {
    return $this->username;
  }

  public function getComment(): string
  {
    return $this->comment;
  }

  public function getCreated(): DateTime
  {
    return $this->created;
  }


}