<?php

namespace Tests\BNNVARA\Comment\Unit\Domain\ValueObject;

use BNNVARA\Comment\Domain\ValueObject\CommentCreate;
use DateTime;
use PHPUnit\Framework\TestCase;

class CommentCreateTest extends TestCase
{
  /** @test */
  public function aCommentCreateCanBeCreated()
  {
    $comment = 'Dit is mijn hele mooie comment';
    $username = 'mdvbnn';
    $created = new DateTime('now');

    $commentCreate = new CommentCreate(
      $comment, $username, $created
    );

    $this->assertInstanceOf(CommentCreate::class, $commentCreate);
    $this->assertSame($comment, $commentCreate->getComment());
    $this->assertSame($username, $commentCreate->getUsername());
    $this->assertSame($created->format('c'), $commentCreate->getCreated()->format('c'));
  }
}
