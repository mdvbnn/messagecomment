<?php

namespace Tests\BNNVARA\Comment\Unit\Domain\Command;

use BNNVARA\Comment\Domain\Command\CommentCreateCommand;
use BNNVARA\Comment\Domain\ValueObject\CommentCreate;
use PHPUnit\Framework\TestCase;

class CommentCreateCommandTest extends TestCase
{
  /** @test */
  public function aCommentCreateCommandCanBeCreated()
  {
    $commentCreate = $this->getCommentCreateValueObjectMock();

    $commentCreateCommand = new CommentCreateCommand($commentCreate);

    $this->assertInstanceOf(CommentCreateCommand::class, $commentCreateCommand);
    $this->assertInstanceOf(CommentCreate::class, $commentCreateCommand->getData());
    $this->assertSame($commentCreate, $commentCreateCommand->getData());
  }

  private function getCommentCreateValueObjectMock(): CommentCreate
  {
    /** @var CommentCreate $commentCreate */
    $commentCreate = $this
      ->getMockBuilder(CommentCreate::class)
      ->disableOriginalConstructor()
      ->getMock();

    return $commentCreate;
  }
}
